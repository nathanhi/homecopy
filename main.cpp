/*	Copyright (c) 2012 Nathan-J. Hirschauer <nathan-jedidja.hirschauer@wien.gv.at>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy of this
 *	software and associated documentation files (the "Software"), to deal in the
 *	Software without restriction, including without limitation the rights to use, copy,
 *	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *	and to permit persons to whom the Software is furnished to do so, subject to the
 *	following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *	OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Homecopy.h"

#if QT_VERSION < 0x050000
    #include <QtGui/QDialog>
    #include <QApplication>
    #include <QtGui/QMessageBox>
    #include <QMessageBox>
#else
    #include <QtWidgets>
#endif

#include <QStandardItemModel>
#include <QListView>
#include <QFileSystemModel>
#include <QDir>
#include <QTranslator>
#include <QLibraryInfo>

int main( int argc, char* argv[]) {
	QApplication a(argc, argv); /* Initializing a QT-Application */

	// Loading translations
	QTranslator qtTranslator;
	qtTranslator.load("qt_" + QLocale::system().name(),
		 QLibraryInfo::location(QLibraryInfo::TranslationsPath));
	a.installTranslator(&qtTranslator);
	QTranslator HomecopyTranslator;
	HomecopyTranslator.load("homecopy_" + QLocale::system().name());
	a.installTranslator(&HomecopyTranslator);
	
	Homecopy w;					/* Defining the window */
	if (QCoreApplication::arguments().count() < 2) {
		w.show();					/* Showing the window */
	}
	return a.exec();			/* Closing the Application and sending an exit code */
}
