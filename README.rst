========
Homecopy
========
**what**
	Homecopy is an assistant for exporting
	userprofiles to an external/network drive.
	It was written in C++ with Qt and is especially
	optimized for a use with Windows PE_.

**why**
	We needed a tool for internal use to export
	user profiles to an external hard drive from
	Windows PE. All public available tools did either
	not meet our expectations or did cost licensing fees.
	Since I also needed a project for my apprenticeship
	exam, so I had a nice amount of work for ~4 months..

**where**
	Homecopy is intended to work on lots of platforms, but
	since the primary target was Windows I can't guarantee,
	that I'll maintain the UNIX port.

Build Prerequisites
-------------------
To build Homecopy you will at least need
the following software installed:

- GCC4_
- Qt_ 5.0 (or newer)

Build the build tools on Windows
--------------------------------
Building software on Windows does everytime
need some preliminary work, so here is a brief
explanation how to get the build tools ready.

1. Get MinGW_, install it and put it in PATH
2. Download and build Qt_ (qt-everywhere-opensource-src-\*). You probably want a static_ build. After building it, add the bin/ directory to PATH.

How to build
------------

- qmake
- make (Windows: mingw32-make)
- *Only* *on* *Windows:* mingw32-make compress
- To compile the translations: make translate

Where to get depending binaries
-------------------------------

To get the zip and unzip-utils you need to compile
InfoZip_. You also can use another binary to compress
the files - but you may have to modify the parameters.

Due to lack of time I didn't implement a compression
algorithm by myself, but it may be possible that I'll
implement it sometimes.

Release Plan
------------

**Alpha 1**: Implemented GUI

**Milestone 1**: Working compression (with Error-Checking) and UI

**Milestone 2**: Reading out registry (to export SID-Key)

**Milestone 3**: Working encryption

**Beta 1**: Bugfixes+Fixing other platforms (Unix, OSX)

**Beta 2**: Bugfixes+Quality Management

**Release Candidate 1**: Functional translations with qlinguist

**Release Candidate 2**: Implemented Metadata

**RTM**: Signing binary with corporate certificate?

Author
------

:Author: Nathan-J. Hirschauer <nathan-jedidja.hirschauer@wien.gv.at>
:Version: 1.0-rc1 (23-12-12)
:License: MIT_


.. _PE: http://en.wikipedia.org/wiki/Windows_Preinstallation_Environment
.. _GCC4: http://gcc.gnu.org/
.. _Qt: http://qt-project.org/
.. _MinGW: http://mingw.org/
.. _static: http://qt-project.org/wiki/How_to_build_a_static_Qt_version_for_Windows_with_gcc
.. _InfoZip: http://www.info-zip.org/
.. _MIT: http://opensource.org/licenses/mit-license.php
