=========
Changelog
=========

0.1-git*
--------
Git development versions. Includes the complete development of all basic functions.

1.0.0
-----
- First release. Mostly all bugs fixed (`#3 <https://bitbucket.org/nathanhi/homecopy/issue/3>`_, `#1 <https://bitbucket.org/nathanhi/homecopy/issue/1>`_, `#14 <https://bitbucket.org/nathanhi/homecopy/issue/14>`_, `#10 <https://bitbucket.org/nathanhi/homecopy/issue/10>`_, `#11 <https://bitbucket.org/nathanhi/homecopy/issue/11>`_, `#2 <https://bitbucket.org/nathanhi/homecopy/issue/2>`_, `#16 <https://bitbucket.org/nathanhi/homecopy/issue/16>`_, `#5 <https://bitbucket.org/nathanhi/homecopy/issue/5>`_, `#7 <https://bitbucket.org/nathanhi/homecopy/issue/7>`_,  `#13 <https://bitbucket.org/nathanhi/homecopy/issue/13>`_, `#17 <https://bitbucket.org/nathanhi/homecopy/issue/17>`_)

1.1.0
-----
- Configuration file introduced
- Fixed bug `#20 <https://bitbucket.org/nathanhi/homecopy/issue/20>`_

1.1.1
-----
- Port to `Qt 5.0 <http://qt-project.org/>`_
- Windows 8 support added
- Icon added
