/*	Copyright (c) 2012 Nathan-J. Hirschauer <nathan-jedidja.hirschauer@wien.gv.at>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy of this
 *	software and associated documentation files (the "Software"), to deal in the
 *	Software without restriction, including without limitation the rights to use, copy,
 *	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *	and to permit persons to whom the Software is furnished to do so, subject to the
 *	following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *	OTHER DEALINGS IN THE SOFTWARE.
 */
 
#include "Homecopy.h"

Worker::Worker() {
	// Declaring our Workerclass
}

Worker::~Worker() {
	// freemem
}


// Used for creating archives..
void Worker::createArchive() {
	// Initial variable declaration
	QString postfile = userlistViewsel;
	postfile.append(".7z");
	destlistViewsel = destlistViewsel.append(postfile);
	userlistViewsel = userlistViewsel.prepend(homePath);
	emit setinfo(QString(tr("Saving registry")), int(25));
	#if (defined(WIN32))
	if (!regop(false, userlistViewsel)) {
		if (!console) {
			emit msgbox(3, QString(tr("Error")), QString(tr("Error while saving Registry settings! Restoring the backup on another computer will propably not work!")), 2);
			if (!hcpmsgboxret) {
				emit setinfo(QString(tr("Aborted")), int(0));
				return;
			}
		}
		else {
			emit setinfo(QString(tr("Error while saving Registry settings! Restoring the backup on another computer will propably not work!\n")), -1);
			exit(-1);
		}
	}
	
	if (!sb2c) {
		QStringList deltreelist;
		QString temp1 = userlistViewsel;
		temp1.append("\\AppData\\Local\\Temp");
		QString temp2 = userlistViewsel;
		temp2.append("\\AppData\\Local\\Temporary Internet Files\\Content.IE5");
		QString temp3 = userlistViewsel;
		temp3.append("\\AppData\\Local\\Temporary Internet Files\\Low\\Content.IE5");
		deltreelist << temp1 << temp2 << temp3;
		// This error message is not helpful atm..
		/*if (!deltree(deltreelist)) {
			if (!console) {
				sem.acquire(2);
				emit msgbox(3, QString(tr("Error")), QString(tr("An error occured while deleting temporary files. Saving and restoring may take more time..")), 0);
				while (sem.available() != 2) {
					usleep(100);
				}
			}
			else {
				emit setinfo(QString(tr("An error occured while deleting temporary files. Saving and restoring may take more time..\n")), -1);
				exit(-1);
			}
		}*/
	}
	QString program = "7za.exe a -bd -mx9 -mmt -t7z -xr!Anwendungsdaten -y -P";
	#elif (defined(__unix__))
	QString program;
	#endif
	emit setinfo(QString(tr("Compressing Userarchive")), int(50));
	/* We don't want to save the
	 * password as cleartext, we will
	 * have to convert the QString to
	 * a QByteArray.. But not today :D
	 */
	program.append(password).append(" ");
	program.append(destlistViewsel).append(" ").append(userlistViewsel);
	QString sevenzipout;
	
	/* Creating a new QProcess
	 * to spawn 7za.exe (yuk..)
	 */
	QProcess *sevenzipProcess = new QProcess(this);
	sevenzipProcess->start(program);
	sevenzipProcess->waitForFinished(-1);
	
	/* If sevenzipProcess ended normally
	 * we can proceed with creating
	 * an archive.
	 */
	if (sevenzipProcess->exitStatus() == QProcess::NormalExit) {
		emit setinfo(QString(tr("Creating checksum")), int(65));
	}
	else {
		if (!console) {
			emit msgbox(3, QString(tr("Error")), QString(tr("Error: Sub-Process returned an error code. Backup will be aborted")), 0);
			emit setinfo(QString(tr("Aborted")), 0);
			return;
		}
		else {
			emit setinfo(QString(tr("Error: Sub-Process returned an error code. Backup will be aborted")), -1);
			exit(-1);
		}
	}
	
	// We need those for the checksum
	QFile sevenzipfile(destlistViewsel);
	QString sha1filelocation = destlistViewsel;
	QFile sha1file(sha1filelocation.append(".sha1"));
	emit setinfo(QString(tr("Creating checksum\nOpening archive...")), int(70));
	
	// Creating Checksum
	if (sevenzipfile.open(QIODevice::ReadOnly)) {
		emit setinfo(QString(tr("Creating checksum\nCreating SHA1-file")), int(80));
		if (sha1file.open(QIODevice::WriteOnly)) {
			emit setinfo(QString(tr("Creating checksum\nCalculating...")), int(80));
			QTextStream sha1fileout(&sha1file);
			QCryptographicHash crypto(QCryptographicHash::Sha1);
			while(!sevenzipfile.atEnd()) {
				crypto.addData(sevenzipfile.read(8192));
			}
			QByteArray hashData = crypto.result();
			sha1fileout << QString(hashData.toHex());
			if (ss.size() > 0) {
				sha1fileout << "----" << ss;
			}
			sha1file.close();
		}
		sevenzipfile.close();
		emit setinfo(QString(tr("Finished!")), int(100));
	}
	if (sb1c) {
		Worker::shutdown();
	}
	emit finished();
}

// Extracting archives or checking it..
void Worker::extractArchive() {
	// Initial declarations
	emit setinfo(QString(tr("Opening SHA1-File")), int(25));
	QFile sevenzipfile(sevenzippath);
	QString sha1Filestring = sevenzippath;
	sha1Filestring.append(".sha1");
	QFile sha1File(sha1Filestring);
	QString sha1sum;
	QStringList decompressedfiles;
	bool sha1file_errorb = false;
	//qDebug() << sha1Filestring;

	// Opening the sha1File..
	if (sha1File.exists() && sha1File.open(QIODevice::ReadOnly))
	{
		QTextStream stream( &sha1File );
		sha1sum = stream.readLine();
		sha1sum = sha1sum.left(40 /*SHA1 consists of 40 chars..*/);
		sha1File.close();
		sha1file_errorb = false;
	}
	else {
		if (sb3c) {
			emit msgbox(3, QString(tr("Error")), QString(tr("Error while opening the list with checksums")), 0);
			emit setinfo(QString(tr("List with checksums could not\nbe opened.")), 0);
			return;
		}
		else {
			emit msgbox(3, QString(tr("Error")), QString(tr("Error while opening the list with checksums")), 2);
			if (!hcpmsgboxret) {
				emit setinfo(QString(tr("Aborted")), int(0));
				return;
			}
			sha1file_errorb = true;
		}
	}
	
	// Opening sevenzipfile if sha1file exists
	emit setinfo(QString(tr("Validating 7z-File")), int(35));
	if (sevenzipfile.open(QIODevice::ReadOnly)) {
		if (sha1file_errorb == false) {
			// Setting the hash algorithm
			QCryptographicHash crypto(QCryptographicHash::Sha1);
			
			/* Looping through the sevenzipfile
			 * and adding the data to our
			 * crypto-algorithm
			 */
			while(!sevenzipfile.atEnd()) {
				crypto.addData(sevenzipfile.read(8192));
			}
			
			/* Getting the result of the hash-calculation
			 * and converting it to hex
			 */
			QByteArray hashData = crypto.result();
			QString sevenzipsha1 = hashData.toHex();
			userdirPath.prepend(homePath); // "lanfoobar".prepend("D:\\Benutzer")
			
			/* If the calculated sha1sum is the same
			 * as the one read from sha1File
			 * we start extracting
			 */
			if (sha1sum == sevenzipsha1) {
				if (sb3c) {
					if (sb1c) {
						Worker::shutdown();
					}
					emit setinfo(QString(tr("Success!")), 100);
					emit msgbox(1, QString(tr("Profile backup checked")), QString(tr("The backup has been checked and there were no errors found.")), 0);
					return;
				}
				emit setinfo(QString(tr("Decompressing archive")), int(65));
				//decompressedfiles = JlCompress::extractDir(sevenzippath, userdirPath);
				Worker::decompress(sevenzippath, userdirPath);
			}
			else {
				if (sb3c) {
					if (!console) {
						emit msgbox(2, QString(tr("Checksum error")), QString(tr("sha1-Sums do not match.\n\nsha1: %1\nSaved sha1: %2")).arg(sevenzipsha1).arg(sha1sum), 0);
						emit setinfo(QString(tr("Checksum error")), 0);
						return;
					}
					else {
						emit setinfo(QString(tr("sha1-Sums do not match.\n\nsha1: %1\nSaved sha1: %2")).arg(sevenzipsha1).arg(sha1sum), -1);
						exit(-1);
					}
				}
				if (!console) {
					emit msgbox(2, QString(tr("Checksum error")), QString(tr("sha1-Sums do not match.\n\nsha1: %1\nSaved sha1: %2")).arg(sevenzipsha1).arg(sha1sum), 2);
					if (hcpmsgboxret) {
						emit setinfo(QString(tr("Decompressing archive")), 65);
						Worker::decompress(sevenzippath, userdirPath);
					}
					else {
						emit setinfo(QString(tr("Aborted by user..")), 0);
						return;
					}
				}
				else {
					emit setinfo(QString(tr("sha1-Sums do not match.\n\nsha1: %1\nSaved sha1: %2")).arg(sevenzipsha1).arg(sha1sum), -1);
				}
			}
		}
	}
	else {
		// If we can't open the sevenzipfile...
		if (!console) {
			emit msgbox(2, QString(tr("Archive could not be opened")), QString(tr("Error: Archive could not be opened. It might be locked by another program. Try anyway?")), 2);
			if (hcpmsgboxret) {
				emit setinfo(QString(tr("Decompressing archive")), 65);
				Worker::decompress(sevenzippath, userdirPath);
			}
			else {
				emit setinfo(QString(tr("Aborted by user..")), 0);
				return;
			}
		}
		else {
			emit setinfo(QString(tr("Error: Archive could not be opened. It might be locked by another program.")), -1);
			exit(-1);
		}
	}
	#if (defined(WIN32))
	//qDebug() << userdirPath;
	if (!regop(true, userdirPath)) {
		if (!console) {
			emit msgbox(3, QString(tr("Error")), QString(tr("Error while restoring Registry settings! Your profile will propably be corrupt!")), 2);
			if (!hcpmsgboxret) {
				emit setinfo(QString(tr("Aborted")), int(0));
				return;
			}
		}
		else {
			emit setinfo(QString(tr("Error while restoring Registry settings! Your profile will propably be corrupt!")), -1);
		}
	}
	#endif
	emit setinfo(QString(tr("Finished")), int(100));
	if (sb1c) {
		Worker::shutdown();
	}
	emit finished();
}

// Spawns a sub process to decompress archives..
void Worker::decompress(QString sevenzippath, QString userdirPath) {
	QString program = "7za.exe x -t7z -bd -y -P";
	program.append(password).append(" ").append(sevenzippath).append(" -o").append(homePath);
	//program.append(userdirPath.mid(0,3)).append(" ").append(sevenzippath);
	/* Creating a new QProcess
	 * to spawn 7za.exe (yuk..)
	 */
	QProcess *sevenzipProcess = new QProcess(this);
	sevenzipProcess->start(program);
	sevenzipProcess->waitForFinished(int(-1));
}

// Checking connection to LDAP-Server..
void Worker::ldap_checkconn() {
	/* Windows PE can't handle LDAP_SSL
	 * So: No SSL-Support :(
	 * 
	 * LDAP Return values:
	 * http://msdn.microsoft.com/en-us/library/windows/desktop/aa367014(v=vs.85).aspx
	 */
	#if (defined(WIN32))
		LDAP *ld;

		//QString hostname = "hostname";
		//QString username = "domain\\user";
		//QString password = "pw";
		
		wchar_t *wchostname = new wchar_t[hostname.size()+1];
		wchar_t *wcusername = new wchar_t[username.size()+1];
		wchar_t *wcpassword = new wchar_t[password.size()+1];
		
		// Converting QString to wchar_t
		hostname.toWCharArray(wchostname);
		username.toWCharArray(wcusername);
		password.toWCharArray(wcpassword);
		// We need to null-terminate our wchar
		wcpassword[password.size()] = L'\0';
		wcusername[username.size()] = L'\0';
		wchostname[hostname.size()] = L'\0';

		ULONG version = LDAP_VERSION3;
		int returncode = 0;
		
		ld = ldap_init(wchostname, LDAP_PORT);
		if (ld == NULL) {
			//char hr = HRESULT_FROM_WIN32(GetLastError());
			//std::cout << "ldap_init failed: " << hr;
			//return -1;
		}
		
		returncode = ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, (void*)&version);
		if (returncode != LDAP_SUCCESS) {
			//std::cout << "set_option failed: " << returncode;
			//return -1;
		}
		
		returncode = ldap_connect(ld, NULL);
		if (returncode != LDAP_SUCCESS) {
			//std::cout << "ldap_connect failed: " << returncode;
			//return -1;
		}
		
		returncode = ldap_simple_bind_s(ld, wcusername, wcpassword);
		if (returncode != LDAP_SUCCESS) {
			if (returncode == 0x51 || returncode == 0x34 || returncode == 0x55) {
				ldapret = 0x03;
			}
			else {
				ldapret = 0x02;
			}
		}
		else {
			ldapret = 0x01;
		}
		
		ldap_unbind(ld);
		delete[] wcusername;
		delete[] wcpassword;
		delete[] wchostname;
		
	#elif (defined(__unix__))
		LDAP *ld;
		char *ldap_host = "ldap://hostname"; // TBD
		int auth_method = LDAP_AUTH_SIMPLE;
		int desired_version = LDAP_VERSION3;
		char *root_dn   = "domain\\user"; // TBD
		char *root_ps   = "pw"; // TBD
		//int result;
		/*result = */ldap_initialize(&ld, ldap_host);
		//cout << "result: " << result << endl;
		/*result = */ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &desired_version);
		//cout << "result: " << result << endl;
		/*result = */ldap_bind_s(ld, root_dn, root_ps, auth_method);
		//cout << "result: " << result << endl;
		ldap_unbind(ld);
	#endif
	emit finished();
}
#if (defined(WIN32))
/* Only used on Windows to
 * get users SID and export the
 * ProfileList
 */
bool Worker::regop(bool optype/*0=compress,1=extract*/, QString userdirPath) {
	// Variable initialization
	TOKEN_PRIVILEGES tp;
	HANDLE hToken;
	LUID luid;
	QString regbackPath;
	
	regbackPath = userdirPath;
	regbackPath.append("\\registry_backup.dat");
	wchar_t *RegBackPath = new wchar_t[regbackPath.size()+1];
	regbackPath.toWCharArray(RegBackPath);
	RegBackPath[regbackPath.size()] = L'\0';
	
	// Setting privileges for registry operations
	OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &hToken);
	LookupPrivilegeValue(NULL, SE_RESTORE_NAME, &luid);
	
	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	AdjustTokenPrivileges(hToken, FALSE, &tp, sizeof(TOKEN_PRIVILEGES), NULL, NULL);
	if (GetLastError() != ERROR_SUCCESS) {
		CloseHandle(hToken);
		//qDebug() << "Error while setting SE_RESTORE_NAME" << GetLastError();
		return false;
	}
	CloseHandle(hToken);
	
	// Setting privileges for registry operations
	OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &hToken);
	LookupPrivilegeValue(NULL, SE_BACKUP_NAME, &luid);
	
	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	AdjustTokenPrivileges(hToken, FALSE, &tp, sizeof(TOKEN_PRIVILEGES), NULL, NULL);
	if (GetLastError() != ERROR_SUCCESS) {
		CloseHandle(hToken);
		//qDebug() << "Error while setting SE_BACKUP_NAME" << GetLastError();
		return false;
	}
	CloseHandle(hToken);
	
	// Loading hive
	LONG loadkey = RegLoadKey(HKEY_USERS, L"homecopy", L"C:\\WINDOWS\\SYSTEM32\\CONFIG\\SOFTWARE"/*\\Microsoft\\WindowsNT\\CurrentVersion\\ProfileList"*/);
	if (loadkey != ERROR_SUCCESS) {
		//qDebug() << "Loading of 'C:\\WINDOWS\\SYSTEM32\\CONFIG\\SOFTWARE' failed!";
		return false;
	}
	if (!optype /*optype == false -> compress*/) {
		/* First of all we need to
		 * get the users SID..
		 */
		userdirPath.append("\\ntuser.dat"); // D:\\Benutzer\\foo\\ntuser.dat
		
		// Variable declaration
		HKEY hKey;
		HANDLE hFile;
		DWORD dwRtnCode = 0;
		PSID pSidOwner = NULL;
		LPTSTR StringSid;
		LPTSTR profilepath = (LPTSTR) TEXT("homecopy\\Microsoft\\Windows NT\\CurrentVersion\\ProfileList\\");
		
		// Opening ntuser.dat
		hFile = CreateFile((LPCTSTR)userdirPath.toStdWString().c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile == INVALID_HANDLE_VALUE) {
			//qDebug() << "Error opening '" << (LPCTSTR)userdirPath.toStdWString().c_str() << "': " << GetLastError();
			return false;
		}

		// Getting the owner SID of the file
		dwRtnCode = GetSecurityInfo(hFile, SE_FILE_OBJECT, OWNER_SECURITY_INFORMATION, &pSidOwner, NULL, NULL, NULL, NULL);
		if (dwRtnCode == ERROR_SUCCESS) {
			if (!ConvertSidToStringSid(pSidOwner, &StringSid)) {
				qDebug("ConvertSidToStringSid failed.");
				return false;
			}
		}
		else {
			//qDebug() << "dwRtnCode: " << dwRtnCode;
			//qDebug() << "Error: " << dwRtnCode << "\n";
			return false;
		}
		
		ss = QString::fromWCharArray(StringSid);
		QString pp;
		pp = QString::fromWCharArray(profilepath);
		QString sidpath;
		sidpath.append(pp).append(ss);
		//qDebug() << "pp: " << pp;
		//qDebug() << "ss: " << ss;
		
		/*wchar_t *sidPath = new wchar_t[sidpath.size()];
		sidpath.toWCharArray(sidPath);
		sidPath[sidpath.size()+1] = L'\0'; // We need to null-terminate our wchar*/
		
		RegOpenKeyEx(HKEY_USERS, (LPCTSTR)sidpath.utf16(), 0, KEY_ALL_ACCESS, &hKey);
		//qDebug() << "LPTSTR: " << sidPath;
		//qDebug() << "QString: " << sidpath;

		LONG regsave = RegSaveKey(hKey, RegBackPath, NULL);
		if (regsave != ERROR_SUCCESS) {
			//qDebug() << "Error saving key: " << regsave;
			return false;
		}
		RegCloseKey(hKey); // Close hKey
	}
	else { // Extract
		// Variable declaration
		QString sha1Filestring = sevenzippath;
		QString StringSid = NULL;
		QFile sha1file(sha1Filestring.append(".sha1"));
		//qDebug() << "sha1Filestring: " << sha1Filestring;
		if (sha1file.open(QIODevice::ReadOnly)) {
			QTextStream sha1fileout(&sha1file);
			StringSid = sha1fileout.readLine();
			sha1file.close();
		}
		//qDebug() << "StringSid: " << StringSid;
		//qDebug() << "indexOf(''----''): " << StringSid.indexOf(QString("\n"));
		//qDebug() << "SHA1: " << StringSid.left(40);
		StringSid = StringSid.right(StringSid.size()-StringSid.indexOf(QString("----"))-4);
		//qDebug() << "Users SID: " << StringSid;
		HKEY hKey;
		HKEY hKey2;
		StringSid.prepend("homecopy\\Microsoft\\Windows NT\\CurrentVersion\\ProfileList\\"); // StringSid contains the SID..
		//qDebug() << StringSid;
		//qDebug() << "Creating Key: HKU\\" << StringSid;
		RegCreateKey(HKEY_USERS, (LPCTSTR)StringSid.utf16(), &hKey2);
		//qDebug() << "Opening HKU\\" << StringSid;
		RegOpenKeyEx(HKEY_USERS, (LPCTSTR)StringSid.utf16(), 0, KEY_ALL_ACCESS, &hKey);
		//qDebug() << "Restoring Key from '" << RegBackPath << "' to hKey";
		LONG regrestore = RegRestoreKey(hKey, RegBackPath, 0x00000008L);
		if (regrestore != ERROR_SUCCESS) {
			//qDebug() << "Error restoring registry hive: " << regrestore;
			return false;
		}
		
		RegCloseKey(hKey); // Close hKey
		RegCloseKey(hKey2); // Close hKey2
		
		if (!DeleteFile(RegBackPath)) { //Delete registry_backup.dat
			//qDebug() << "Error deleting registry_backup.dat";
			return false;
		}
	}

	// Unloading hive
	LONG unloadhomecopy = RegUnLoadKey(HKEY_USERS, L"homecopy");
	if (unloadhomecopy != ERROR_SUCCESS) {
		//qDebug() << "Error while unloading HKU\\homecopy: " << unloadhomecopy;
		return false;
	}
	return true;
}
#endif

// Shuts down the computer..
void Worker::shutdown() {
	#if (defined(WIN32))
	// Initializing all variables
	TOKEN_PRIVILEGES shutdown_tp;
	HANDLE shutdownToken;
	LUID shutdownluid;
	
	// Setting privileges for registry operations
	OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &shutdownToken);
	LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &shutdownluid);
	
	shutdown_tp.PrivilegeCount = 1;
	shutdown_tp.Privileges[0].Luid = shutdownluid;
	shutdown_tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	AdjustTokenPrivileges(shutdownToken, FALSE, &shutdown_tp, sizeof(TOKEN_PRIVILEGES), NULL, NULL);
	
	if (GetLastError() != ERROR_SUCCESS) {
		CloseHandle(shutdownToken);
		return;
	}
	CloseHandle(shutdownToken);
	InitiateSystemShutdown(NULL, (LPTSTR) TEXT("Automatically initiated shutdown after user profile operation with Homecopy"), 0, false, false);
	return;
	#elif (defined(__unix__))
	#include <unistd.h>
	sync(); // Syncing all filesystem operations
	system("halt");
	return;
	#endif
}

// Used for removing directories..
bool Worker::deltree(QStringList locations) {
	bool ret = false;
    foreach (QString location, locations) {
		QString location_old = location;
		QDir dir(location);
		
		if (dir.exists(location)) {
			Q_FOREACH(QFileInfo fileInfo, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
				if (fileInfo.isDir()) {
					QStringList fileInfoisdir;
					fileInfoisdir << fileInfo.absoluteFilePath();
					ret = deltree(fileInfoisdir);
				}
				else {
					ret = QFile::remove(fileInfo.absoluteFilePath());
				}
	 
				if (!ret) {
					return ret;
				}
			}
			// We don't want to delete the top directory..
			if (location != location_old) {
				ret = dir.rmdir(location);
			}
		}
    }
    locations.clear();
    return ret;
}
