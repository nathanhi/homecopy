<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="sv_SE">
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../about.ui" line="38"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="81"/>
        <source>&amp;About</source>
        <translation type="unfinished">&amp;Om</translation>
    </message>
    <message>
        <location filename="../about.ui" line="105"/>
        <source>Homecopy</source>
        <translation type="unfinished">Homecopy</translation>
    </message>
    <message>
        <location filename="../about.ui" line="136"/>
        <source>Homecopy is a sophisticated tool to backup and restore user profiles. It&apos;s optimized for usage from a recovery system like Windows PE. Although it is mainly used for Windows profiles the software is compatible with Unixoids (Linux, *BSD, Mac OS X). The software is licensed as MIT and the source code is available via the link 

Developer:

Nathan-J. Hirschauer &lt;nathan-jedidja.hirschauer@wien.gv.at&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="167"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://bitbucket.org/nathanhi/homecopy&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://bitbucket.org/nathanhi/homecopy&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="189"/>
        <source>About &amp;Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="213"/>
        <source>Qt Toolkit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="240"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://qt-project.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://qt-project.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="268"/>
        <source>Qt is a C++ toolkit for cross-platform application development.

Qt provides single-source portability across MS Windows, Mac OS X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.
Qt is available under three different licensing options designed to accommodate the needs of our various users.

Qt is dual-licensed with GPLv3.0 (free) and LGPL v2.0 (commercial).</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Homecopy</name>
    <message>
        <location filename="../Homecopy.cpp" line="239"/>
        <source>Error: Couldn&apos;t find a user directory</source>
        <translation>Fel: Användaremapp kunde inte hittas</translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="92"/>
        <location filename="../Homecopy.cpp" line="240"/>
        <source>Error</source>
        <translation>Fel</translation>
    </message>
    <message>
        <source>End programme when &amp;finished</source>
        <translation type="obsolete">Avsluta programmet efter &amp;slutförandet</translation>
    </message>
    <message>
        <source>&amp;Remove temporary files</source>
        <translation type="obsolete">&amp;Ta bort temporär filer</translation>
    </message>
    <message>
        <source>Only &amp;check the archive</source>
        <translation type="obsolete">Endast &amp;kontrollera arkiv</translation>
    </message>
    <message>
        <source>Please enter a username
and a destination!</source>
        <translation type="obsolete">Välj ett användarnamn och/eller mål!</translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="92"/>
        <source>Error while loading config file &apos;homecopy.ini&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="251"/>
        <source>Shutdown when
&amp;finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="252"/>
        <source>&amp;Don&apos;t remove
temporary files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="253"/>
        <source>Only &amp;check
the archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="254"/>
        <location filename="../Homecopy.cpp" line="516"/>
        <source>Destination</source>
        <translation>Mål</translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="255"/>
        <location filename="../Homecopy.cpp" line="529"/>
        <source>Please enter a username and a destination!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="534"/>
        <source>Source</source>
        <translation>Källa</translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="535"/>
        <source>Please select a source and username!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="699"/>
        <source>Preparing backup</source>
        <translation>Förbereda säkring</translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="710"/>
        <location filename="../Homecopy.cpp" line="768"/>
        <source>Enter password for </source>
        <translation>Ange lösenordet för </translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="714"/>
        <location filename="../Homecopy.cpp" line="772"/>
        <source>Please enter the password of </source>
        <translation>Ange lösenordet av   </translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="716"/>
        <location filename="../Homecopy.cpp" line="774"/>
        <source> again. The password was wrong.</source>
        <translation>igen. Den lösenordet var felaktigt.</translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="718"/>
        <location filename="../Homecopy.cpp" line="776"/>
        <source>Enter password</source>
        <translation>Ange lösenord</translation>
    </message>
    <message>
        <source>No user and/or destination selected</source>
        <translation type="obsolete">Ingen användarnamn och/eller mål valts</translation>
    </message>
    <message>
        <source>Please select a user and/or destination from the list.</source>
        <translation type="obsolete">Välj ett användarnamn och/eller mål.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Okej</translation>
    </message>
    <message>
        <source>No user and/or
destination selected.</source>
        <translation type="obsolete">Ingen användarnamn och/eller mål valts.</translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="748"/>
        <source>Preparing check</source>
        <translation>Förbereda kontroll</translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="752"/>
        <source>Preparing restore</source>
        <translation>Förbereda Återskapa</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../homecopy.ui" line="29"/>
        <source>Homecopy</source>
        <translation>Homecopy</translation>
    </message>
    <message>
        <location filename="../homecopy.ui" line="45"/>
        <source>Username</source>
        <translation>Användernamn</translation>
    </message>
    <message>
        <location filename="../homecopy.ui" line="83"/>
        <source>Source/Destination</source>
        <translation>Källa/Mål</translation>
    </message>
    <message>
        <location filename="../homecopy.ui" line="108"/>
        <source>&amp;Start</source>
        <translation>&amp;Start</translation>
    </message>
    <message>
        <location filename="../homecopy.ui" line="121"/>
        <source>Advanced Settings</source>
        <translation>Avancerade Inställningar</translation>
    </message>
    <message>
        <location filename="../homecopy.ui" line="173"/>
        <source>Save</source>
        <translation>Säkra</translation>
    </message>
    <message>
        <location filename="../homecopy.ui" line="189"/>
        <source>Restore</source>
        <translation>Återställ</translation>
    </message>
    <message>
        <location filename="../homecopy.ui" line="202"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../homecopy.ui" line="233"/>
        <source>Se&amp;ttings</source>
        <translation>Ins&amp;tällningar</translation>
    </message>
    <message>
        <location filename="../homecopy.ui" line="245"/>
        <source>&amp;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../homecopy.ui" line="265"/>
        <source>&amp;About</source>
        <translation>&amp;Om</translation>
    </message>
    <message>
        <location filename="../homecopy.ui" line="260"/>
        <source>Show advanced &amp;options</source>
        <translation>Visa avancerade &amp;Inställningar</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Homecopy.cpp" line="493"/>
        <source>Continue</source>
        <translation>Fortsätta</translation>
    </message>
    <message>
        <location filename="../Homecopy.cpp" line="494"/>
        <source>Abort</source>
        <translation>Avbryta</translation>
    </message>
</context>
<context>
    <name>Worker</name>
    <message>
        <location filename="../worker.cpp" line="39"/>
        <source>Saving registry</source>
        <translation>Spara registry</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="44"/>
        <location filename="../worker.cpp" line="113"/>
        <location filename="../worker.cpp" line="182"/>
        <location filename="../worker.cpp" line="190"/>
        <location filename="../worker.cpp" line="306"/>
        <source>Error</source>
        <translation>Fel</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="44"/>
        <source>Error while saving Registry settings! Restoring the backup on another computer will propably not work!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="49"/>
        <location filename="../worker.cpp" line="117"/>
        <location filename="../worker.cpp" line="195"/>
        <location filename="../worker.cpp" line="311"/>
        <source>Aborted</source>
        <translation>Avbruten</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="54"/>
        <source>Error while saving Registry settings! Restoring the backup on another computer will propably not work!
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="86"/>
        <source>Compressing Userarchive</source>
        <translation>Komprimera Arkiv</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="108"/>
        <source>Creating checksum</source>
        <translation>Skapa kontrollsumma</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="113"/>
        <location filename="../worker.cpp" line="121"/>
        <source>Error: Sub-Process returned an error code. Backup will be aborted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="130"/>
        <source>Creating checksum
Opening archive...</source>
        <translation>Skapa kontrollsumma
Öppna arkiv...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="134"/>
        <source>Creating checksum
Creating SHA1-file</source>
        <translation>Skapa kontrollsumma
Skapa kontrollsumfil</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="136"/>
        <source>Creating checksum
Calculating...</source>
        <translation>Skapa kontrollsumma
Beräkna...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="150"/>
        <source>Finished!</source>
        <translation>Färdig!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="160"/>
        <source>Opening SHA1-File</source>
        <translation>Öppna kontrollsumfil</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="182"/>
        <location filename="../worker.cpp" line="190"/>
        <source>Error while opening the list with checksums</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="186"/>
        <source>List with checksums could not
be opened.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="203"/>
        <source>Validating 7z-File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Validating ZIP-File</source>
        <translation type="obsolete">Validera arkiv</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="233"/>
        <source>Success!</source>
        <translation>Framgång!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="234"/>
        <source>Profile backup checked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="234"/>
        <source>The backup has been checked and there were no errors found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="237"/>
        <location filename="../worker.cpp" line="265"/>
        <location filename="../worker.cpp" line="288"/>
        <source>Decompressing archive</source>
        <translation>Återställa arkiv</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="247"/>
        <location filename="../worker.cpp" line="251"/>
        <location filename="../worker.cpp" line="260"/>
        <source>Checksum error</source>
        <translation>Kontrollsumfel</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="247"/>
        <location filename="../worker.cpp" line="255"/>
        <location filename="../worker.cpp" line="260"/>
        <location filename="../worker.cpp" line="274"/>
        <source>sha1-Sums do not match.

sha1: %1
Saved sha1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="269"/>
        <location filename="../worker.cpp" line="292"/>
        <source>Aborted by user..</source>
        <translation>Avbröts av Användaren..</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="283"/>
        <source>Archive could not be opened</source>
        <translation>Arkiv kunde inte öppnas</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="283"/>
        <source>Error: Archive could not be opened. It might be locked by another program. Try anyway?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="297"/>
        <source>Error: Archive could not be opened. It might be locked by another program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="306"/>
        <location filename="../worker.cpp" line="316"/>
        <source>Error while restoring Registry settings! Your profile will propably be corrupt!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="320"/>
        <source>Finished</source>
        <translation>Färdig!</translation>
    </message>
</context>
</TS>
