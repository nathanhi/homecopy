/*	Copyright (c) 2012 Nathan-J. Hirschauer <nathan-jedidja.hirschauer@wien.gv.at>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy of this
 *	software and associated documentation files (the "Software"), to deal in the
 *	Software without restriction, including without limitation the rights to use, copy,
 *	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *	and to permit persons to whom the Software is furnished to do so, subject to the
 *	following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *	OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Homecopy.h"
#include "about.h"

/* Checking defines and setting variables
 * according to the target system
 */

// Declaring general variables
QThread* createArchivethread;
Worker* createArchiveworker;
QThread* extractArchivethread;
Worker* extractArchiveworker;
QThread* ldap_checkconnthread;
Worker* ldap_checkconnworker;

QString homePath;
QString domainname;
QString currentPath;
QString destlistViewsel;
QString userlistViewsel;
QString sevenzippath;
QString userdirPath;
QString hostname;
QString username;
QString password;
QString ss;
QString userlistViewsel_old;
QString destlistViewsel_old;

bool hcpmsgboxret;

int progressBarValue;
int count_ulistclicked = 0;
int count_dlistclicked = 0;
int ldapret = 0x00;
/* Return codes of ldapret:
 * 		- 0x00 -> Nothing set
 * 		- 0x01 -> Login successful
 * 		- 0x02 -> Login unsuccessful/Password wrong
 * 		- 0x03 -> No connection possible
 * 				--> Server down
 * 				--> Network down
 * 				--> Server busy
 * 				--> Connection timeout
 */

bool sb1c = false;	
bool sb2c = false;
bool sb3c = false;
bool restore;
bool console;
bool running;

Homecopy::Homecopy(QMainWindow *parent) : QMainWindow(parent) {
	// Checking for an existing settings file
	QFile settingsFile("homecopy.ini");
	if (settingsFile.exists()) {
		QSettings settings(QString("homecopy.ini"), QSettings::IniFormat);
		domainname = settings.value("main/domain", 0).toString();
		hostname = settings.value("main/ldaphost", 0).toString();
		homePath = settings.value("main/homedir", 0).toString();
		if (!domainname.endsWith("\\")) {
			domainname.append("\\");
		}
	}
	// When we can't find any we drop an error message..
	else {
		hcpmsgbox(3, QString(tr("Error")), QString(tr("Error while loading config file 'homecopy.ini'")), 0);
		exit(-1);
	}
	
	/* If we have more than one argument
	 * (the first one is always the program itself)
	 * we handle it as a console application..
	 * 
	 * Not exactly a fine way - It should be possible
	 * to detect if it was launched from a shell..
	 * TBD
	 */
	if (QCoreApplication::arguments().count() > 1) {
		bool save = false;
		bool restore2 = false;
		QStringList args = QCoreApplication::arguments();
		console = true;
		int i;
		// Iterate through every argument and parse it
		for (i=1; i < args.count(); i++) {
			// Save userprofile
			if (args.at(i) == "-s" || args.at(i) == "--save") {
				save = true;
			}
			// Restore Userprofile
			else if (args.at(i) == "-r" || args.at(i) == "--restore") {
				restore2 = true;
			}
			// Check Userprofile
			else if (args.at(i) == "-c" || args.at(i) == "--check") {
				sb3c = true;
			}
			// Shutdown after completion
			else if (args.at(i) == "-S" || args.at(i) ==  "--shutdown") {
				sb1c = true;
			}
			// Don't remove temporary files
			else if (args.at(i) == "-n" || args.at(i) == "--noremove") {
				sb2c = true;
			}
			// Print version and exit
			else if (args.at(i) == "-v" || args.at(i) == "--version") {
				printf("Homecopy %i.%i.%i\n", VERSION, PATCHLEVEL, SUBLEVEL);
				exit(0);
			}
			// Show help
			else if (args.at(i) == "-h" || args.at(i) == "--help") {
				help();			
			}
			// Username
			else if (args.at(i).startsWith("-u=")) {
				userlistViewsel = args.at(i);
				userlistViewsel.remove(0, 3);
			}
			// Username
			else if (args.at(i).startsWith("--username=")) {
				userlistViewsel = args.at(i);
				userlistViewsel.remove(0, 11);
			}
			// Destionation
			else if (args.at(i).startsWith("-d=")) {
				destlistViewsel = args.at(i);
				destlistViewsel.remove(0, 3);
			}
			// Destionation
			else if (args.at(i).startsWith("--destination=")) {
				destlistViewsel = args.at(i);
				destlistViewsel.remove(0, 14);
			}
			// Source / Origin
			else if (args.at(i).startsWith("-o=")) {
				destlistViewsel = args.at(i);
				destlistViewsel.remove(0, 3);
			}
			// Source / Origin
			else if (args.at(i).startsWith("--origin=")) {
				destlistViewsel = args.at(i);
				destlistViewsel.remove(0, 9);
			}
		}
		
		/* Error-Handler for multiple actions
		 * (e.g. running save and restore at
		 * the same time)
		 */
		if ((save && restore2) || (save && sb3c) || (restore2 && sb3c)) {
			printf("ERROR: Multiple actions at one time are not supported! You must either use '-s', '-r' **or** '-c'!\n\n");
			help();
		}
		
		// If save has been selected start the save-routine
		if (save) {
			restore = false;
			if(userlistViewsel.isEmpty()) {
				printf("ERROR: You didn't enter a username to save!\n");
				help();
			}
			if(destlistViewsel.isEmpty()) {
				printf("ERROR: You didn't enter a destination path for the backup!\n");
				help();
			}
			QString userlistViewsel2 = userlistViewsel;
			userlistViewsel2 = userlistViewsel2.prepend(homePath);
			if (!QDir(userlistViewsel2).exists()) {
				printf("ERROR: The username you have entered is invalid!\n");
				help();
			}
			if (!QDir(destlistViewsel).exists()) {
				printf("ERROR: The destination you have entered is invalid!\n");
				help();
			}
			init();
			start();
		}
		
		// If restore has been selected start the restore-routine
		else if (restore2) {
			restore = true;
			if(userlistViewsel.isEmpty()) {
				printf("ERROR: You didn't enter an archive!\n");
				help();
			}
			if(destlistViewsel.isEmpty()) {
				printf("ERROR: You didn't enter a source path of the backup!\n");
				help();
			}
			
			QString userlistViewsel2 = userlistViewsel;
			userlistViewsel2 = userlistViewsel2.prepend(destlistViewsel);
			if (!QFile(userlistViewsel2).exists()) {
				printf("ERROR: The file name you have entered is invalid!\n");
				help();
			}
			init();
			start();
		}
	}
	else {
		/*
		 * If we don't have parameters,
		 * we don't want to see the console
		 * window ... DIRTY :D
		 * TBD
		 */
		#if defined(WIN32)
		HWND hwnd = GetConsoleWindow();
		ShowWindow(hwnd, 0);
		#endif
		console = false;
		setupUi(this); // Setting up the UI
		init();	// Executing Homecopy::init()
	}
}

Homecopy::~Homecopy() { }

/*
 * This function initializes the
 * our UI and contains all
 * connectors for various events
 */
 
void Homecopy::init() {
	/* If we are on console,
	 * we don't need multiple
	 * things like variables
	 * for the UI and so on..
	 */
	if (!console) {
		// Drop an error when there is no user directory
		if (QDir(homePath).exists() == false) {
			QMessageBox error_nodir;
			QString error_nodir_text = tr("Error: Couldn't find a user directory");
			QString error_nodir_title = tr("Error");
			error_nodir.setIcon(QMessageBox::Critical);
			error_nodir.setText(error_nodir_text);
			error_nodir.setWindowTitle(error_nodir_title);
			error_nodir.exec();
			exit(0);
		}
		
		// Setting some values - Initialization would be FTW!
		settingBox1->setText(tr("Shutdown when\n&finished"));
		settingBox2->setText(tr("&Don't remove\ntemporary files"));
		settingBox3->setText(tr("Only &check\nthe archive"));
		destinationgroupBox->setTitle(tr("Destination"));
		statuslabel->setText(tr("Please enter a username and a destination!"));
		destlistViewsel = destlistViewsel_old;
		userlistViewsel = userlistViewsel_old;
		
		progressBar->setValue(0); // Resetting value of the progressbar to zero
		settingBox1->setEnabled(false);
		settingBox2->setEnabled(false); // Disabling all settingBoxes
		settingBox3->setEnabled(false); // (these are enabled by default)
		groupBox_3->setEnabled(false);
		count_dlistclicked = 0;
		count_ulistclicked = 0;
		startButton->setEnabled(false);
	}
	ldapret = 0x00;
	running = false;
	
	/* We also don't need the
	 * UI-connecters without GUI..
	 */
	if (!console) {
		// All sorts of connectors
		connect(startButton, SIGNAL(clicked()), this, SLOT(start()));
			// If startButton was clicked() run start()
		connect(saveButton, SIGNAL(clicked()), this, SLOT(Events()));
			// If saveButton was clicked() run Events()
		connect(restoreButton, SIGNAL(clicked()), this, SLOT(Events()));
			// If restoreButton was clicked() run Events()
		connect(actionErweiterte_Optionen_anzeigen, SIGNAL(changed()), this, SLOT(Events()));
			// If actionErweiterte_Optionen_anzeigen was changed() run Events()
		connect(action_About, SIGNAL(triggered()), this, SLOT(openabout()));
			// If action_About has been triggered() run Events()
		connect(settingBox1, SIGNAL(toggled(bool)), this, SLOT(Events()));
			// If settingBox1 was toggled() run Events()
		connect(settingBox2, SIGNAL(toggled(bool)), this, SLOT(Events()));
			// If settingBox2 was toggled() run Events()
		connect(settingBox3, SIGNAL(toggled(bool)), this, SLOT(Events()));
			// If settingBox3 was toggled() run Events()
		connect(userlistView, SIGNAL(clicked(const QModelIndex &)), this, SLOT(ulistclicked(const QModelIndex &)));
			// If the user clicked userlistView was clicked() run a special slot (ulistclicked())
			// We need this, because we have to hand const QModelIndex & over
		connect(destlistView, SIGNAL(clicked(const QModelIndex &)), this, SLOT(dlistclicked(const QModelIndex &)));
			// If the user clicked destlistView was clicked() run a special slot (dlistclicked())
			// We need this, because we have to hand const QModelIndex & over
			
		QFileSystemModel *destlist = new QFileSystemModel;
			destlist->setRootPath(QDir::rootPath());
			destlistView->setModel(destlist);
		
		QDirModel *userlist = new QDirModel;
			userlist->setReadOnly(true);
			userlist->setFilter(QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);
			userlist->setSorting(QDir::Name);
			QStringList usernamefilter;
			usernamefilter << "lan*" << "adm*";
			userlist->setNameFilters(usernamefilter);
			userlistView->setModel(userlist);
			userlistView->setRootIndex(userlist->index(homePath));
	}
	
	// Initializing all needed threads
	
	//createArchive()
	createArchivethread = new QThread;
	createArchiveworker = new Worker();
	createArchiveworker->moveToThread(createArchivethread);
	connect(createArchivethread, SIGNAL(started()), createArchiveworker, SLOT(createArchive()));
	connect(createArchiveworker, SIGNAL(finished()), createArchivethread, SLOT(quit()));
	connect(createArchiveworker, SIGNAL(setinfo(QString, int)), this, SLOT(setinfo(QString, int)), Qt::BlockingQueuedConnection);
	connect(createArchiveworker, SIGNAL(finished()), this, SLOT(init()));
	connect(createArchiveworker, SIGNAL(msgbox(int, QString, QString, int)), this, SLOT(hcpmsgbox(int, QString, QString, int)), Qt::BlockingQueuedConnection);
	connect(createArchiveworker, SIGNAL(finished()), createArchiveworker, SLOT(deleteLater()));
	connect(createArchivethread, SIGNAL(finished()), createArchivethread, SLOT(deleteLater()));
	
	//extractArchive()
	extractArchivethread = new QThread;
	extractArchiveworker = new Worker();
	extractArchiveworker->moveToThread(extractArchivethread);
	connect(extractArchivethread, SIGNAL(started()), extractArchiveworker, SLOT(extractArchive()));
	connect(extractArchiveworker, SIGNAL(finished()), extractArchivethread, SLOT(quit()));
	connect(extractArchiveworker, SIGNAL(setinfo(QString, int)), this, SLOT(setinfo(QString, int)), Qt::BlockingQueuedConnection);
	connect(extractArchiveworker, SIGNAL(finished()), this, SLOT(init()));
	connect(extractArchiveworker, SIGNAL(msgbox(int, QString, QString, int)), this, SLOT(hcpmsgbox(int, QString, QString, int)), Qt::BlockingQueuedConnection);
	connect(extractArchiveworker, SIGNAL(finished()), extractArchiveworker, SLOT(deleteLater()));
	connect(extractArchivethread, SIGNAL(finished()), extractArchivethread, SLOT(deleteLater()));
	
	//ldap_checkconn()
	ldap_checkconnthread = new QThread;
	ldap_checkconnworker = new Worker();
	ldap_checkconnworker->moveToThread(ldap_checkconnthread);
	connect(ldap_checkconnthread, SIGNAL(started()), ldap_checkconnworker, SLOT(ldap_checkconn()));
	connect(ldap_checkconnworker, SIGNAL(finished()), ldap_checkconnthread, SLOT(quit()));
	//connect(ldap_checkconnworker, SIGNAL(ldapinfo(int)), this, SLOT(ldapinfo(int)));
	connect(ldap_checkconnworker, SIGNAL(finished()), ldap_checkconnworker, SLOT(deleteLater()));
	connect(ldap_checkconnthread, SIGNAL(finished()), ldap_checkconnthread, SLOT(deleteLater()));
	connect(ldap_checkconnworker, SIGNAL(finished()), this, SLOT(start()));
}

/* This function is called by init() out of an event
 * if an item from userlistView has been selected
 */
void Homecopy::ulistclicked(const QModelIndex & index) {
	// Initializing the main variables
	count_ulistclicked = count_ulistclicked+1;
	QItemSelectionModel *sel = userlistView->selectionModel();
	QModelIndexList list = sel->selectedIndexes();
	QString selectedItem;
	
	/* Checking if count_dlistclicked&&count_ulistclicked >= 2
	 * so that we can enable the start-Button
	 */
	if ((count_dlistclicked >= 1) && (count_ulistclicked >= 1)) {
		startButton->setEnabled(true);
	}
	
	// Iterating through every index in our list
	// to get the text of the selected item
	foreach(QModelIndex index, list) {
		userlistViewsel = index.data().toString();
	}
}

/* This function is called by init() out of an event
 * if an item from destlistView has been selected
 */
void Homecopy::dlistclicked(const QModelIndex & index) {
	// Initializing the main variables
	count_dlistclicked = count_dlistclicked+1;
	QItemSelectionModel *sel = destlistView->selectionModel();
	QModelIndexList list = sel->selectedIndexes();
	QString selectedItem;
	
	/* Checking if count_dlistclicked&&count_ulistclicked >= 2
	 * so that we can enable the start-Button
	 */
	if ((count_dlistclicked >= 1) && (count_ulistclicked >= 1)) {
		startButton->setEnabled(true);
	}
	
	// Iterating through every index in our list
	// to get the text of the selected item
	foreach(QModelIndex index, list) {
		destlistViewsel = index.data().toString();
	}
	
	/* We'll have to parse the destlistView,
	 * since we need some usable drive letters
	 * to compress the files correctly..
	 */

	// Original: destlistViewsel == VBOX_tmp (F:)
	QString colon = destlistViewsel;
	colon = colon.right(1);
	if (colon != QString(":")) {	// If we've got a drive with description
		destlistViewsel.chop(1);	// "VBOX_tmp (F:"
	}
	destlistViewsel = destlistViewsel.right(2);	// "F:"
	destlistViewsel.append("\\");	// "F:\\"
	
	if (restore) {
		userlist->setReadOnly(true);
		userlist->setFilter(QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);
		userlist->setSorting(QDir::Name);
		QStringList usernamefilter;
		usernamefilter << "lan*.7z" << "adm*.7z";
		userlist->setNameFilters(usernamefilter);
		userlistView->setModel(userlist);
		userlistView->setRootIndex(userlist->index(destlistViewsel));
	}
}

/* Routine to update statuslabel
 * progressBarValue - Mainly used
 * by worker.cpp
 */
void Homecopy::setinfo(QString msg, int perc) {
	/* If we are on console,
	 * we want console output
	 */
	if (console) {
		printf("%s\n", qPrintable(msg));
		if (perc == 100) {
			exit(0);
		}
	}
	else {
		statuslabel->setText(msg);
		if (perc != -1) {
			setprogressBarValue(perc);
		}
	}
}

/*// Gets called through the ldapinfo-signal
 * void Homecopy::ldapinfo(int foo) {
 *	ldapret = foo;
 *	Homecopy::start();
 *}
 */

// Gets called if "About" was clicked..
void Homecopy::openabout() {
	AboutDlg *about = new AboutDlg(this);
	about->show();
}

// Showing a messagebox..
void Homecopy::hcpmsgbox(int icon, QString title, QString text, int buttons) {
	QMessageBox msgbox;
	msgbox.setWindowTitle(title);
	msgbox.setText(text);
	// From: http://qt-project.org/doc/qt-4.8/qmessagebox.html#icon-prop
	switch (icon) {
		case 0:
			msgbox.setIcon(QMessageBox::NoIcon);
			break;
		case 1:
			msgbox.setIcon(QMessageBox::Information);
			break;
		case 2:
			msgbox.setIcon(QMessageBox::Warning);
			break;
		case 3:
			msgbox.setIcon(QMessageBox::Critical);
			break;
		case 4:
			msgbox.setIcon(QMessageBox::Question);
			break;
	}
	if (buttons == 2) {
		msgbox.addButton(QObject::tr("Continue"), QMessageBox::AcceptRole);
		msgbox.addButton(QObject::tr("Abort"), QMessageBox::RejectRole);
		int msgboxret = msgbox.exec();
		switch (msgboxret) {
			case QMessageBox::AcceptRole:
				hcpmsgboxret = true;
				break;
			case QMessageBox::RejectRole:
				hcpmsgboxret = false;
				break;
		}
	}
	else {
		msgbox.exec();
	}	
}


// Event-Handler
void Homecopy::Events() {
	/* If the user clicked on 'Save'
	 * we change the UI to the corresponding
	 * functions
	 */
	if (saveButton->isChecked()) {
		destinationgroupBox->setTitle(tr("Destination"));
		restore = false;
		
		QDirModel *userlist = new QDirModel;
			userlist->setReadOnly(true);
			userlist->setFilter(QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);
			userlist->setSorting(QDir::Name);
			QStringList usernamefilter;
			usernamefilter << "lan*" << "adm*";
			userlist->setNameFilters(usernamefilter);

		userlistView->setModel(userlist);
		userlistView->setRootIndex(userlist->index(homePath));
		statuslabel->setText(tr("Please enter a username and a destination!"));
	}
	
	/* If the user clicked on 'Restore'
	 * we change the UI to the corresponding
	 * functions
	 */
	if (restoreButton->isChecked()) {
		destinationgroupBox->setTitle(tr("Source"));
		statuslabel->setText(tr("Please select a source and username!"));
		restore = true;
		
		QDirModel *userlist = new QDirModel;
			userlist->setReadOnly(true);
			userlist->setFilter(QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);
			userlist->setSorting(QDir::Name);
			QStringList usernamefilter;
			usernamefilter << "lan*.7z" << "adm*.7z";
			userlist->setNameFilters(usernamefilter);

		userlistView->setModel(userlist);
		userlistView->setRootIndex(userlist->index(NULL));
	}
	
	/* If the user clicked on 'Advanced options'
	 * we active the setting- and
	 * groupBox..
	 */
	if (actionErweiterte_Optionen_anzeigen->isChecked()) {
		settingBox1->setEnabled(true);
		settingBox2->setEnabled(true);
		settingBox3->setEnabled(true);
		groupBox_3->setEnabled(true);
	}
	
	/* If the user disabled the advanced options
	 * we'll have to untick all settingBoxes
	 */
	if (actionErweiterte_Optionen_anzeigen->isChecked() == false) {
		settingBox1->setEnabled(false);
		settingBox2->setEnabled(false);
		settingBox3->setEnabled(false);
		groupBox_3->setEnabled(false);
		sb1c = false;
		sb2c = false;
		/* If settingBox3 was checked
		 * we have to re-enable the
		 * save-function..
		 */
		if (sb3c) {
			saveButton->setCheckable(true);
			saveButton->setEnabled(true);
		}
		sb3c = false;
	}
	
	/* Some logic to set the variables that indicate
	 * whether the relevant settingBoxes are checked or not
	 */
	if (actionErweiterte_Optionen_anzeigen->isChecked() == true) {
		if ((sb1c == false) && (settingBox1->isChecked())) {
			sb1c = true;
		}
		if ((sb2c == false) && (settingBox2->isChecked())) {
			sb2c = true;
		}
		if ((sb3c == false) && (settingBox3->isChecked())) {
			sb3c = true;
			// Automatically switch to the 'restore'-Tab, when sb3c is checked..
			restoreButton->setChecked(true);
			restoreButton->click();
			// Disabling the save-function..
			saveButton->setCheckable(false);
			saveButton->setEnabled(false);
		}
		if ((sb1c == true) && !(settingBox1->isChecked())) {
			sb1c = false;
		}
		if ((sb2c == true) && !(settingBox2->isChecked())) {
			sb2c = false;
		}
		if ((sb3c == true) && !(settingBox3->isChecked())) {
			sb3c = false;
			// Enabling the save-function again..
			saveButton->setCheckable(true);
			saveButton->setEnabled(true);
		}
	}
}

/* This routine is used to set the root index of
 * destlistView - For showing the contents of a path
 */
void Homecopy::setRootIndex(const QModelIndex& index) {
	QModelIndex dir = index.sibling(index.row(), 0);
	destlistView->setRootIndex(dir);
}

// To make the progressBar move more smoothly..
void Homecopy::setprogressBarValue(int progressBarValue) {
	int i = progressBar->value();
	if (progressBarValue < i) {
		while (i > progressBarValue) {
			i--;
			progressBar->setValue(i);
			usleep(75);
		}
	}
	else {
		while (i < progressBarValue) {
			i++;
			progressBar->setValue(i);
			usleep(75);
		}
	}
}

// Prints a help message to stdout..
void Homecopy::help() {
	printf("Usage: homecopy [OPTIONS]\n\nExamples:\n");
	printf("Save userprofile:\thomecopy -s -u=username -d=F:\\\n");
	printf("Restore userprofile:\thomecopy -r -u=username.7z -o=F:\\\n");
	printf("Check archive:\t\thomecopy -c -u=username.7z -o=F:\\\n\n");
	printf("Parameters:\n");
	printf("-s   --save\t\tSave user profile\n");
	printf("-r   --restore\t\tRestore user profile\n");
	printf("-c   --check\t\tOnly check archive\n");
	printf("-h   --help\t\tShow this help\n");
	printf("-v   --version\t\tPrint version and exit\n\n");
	printf("-S   --shutdown\t\tShutdown after completion\n");
	printf("-n   --noremove\t\tDon't remove temporary files\n\n");
	printf("-u=  --username=\tSpecifying username or backup archive\n");
	printf("-d=  --destination=\tSpecifying destination [Only with -s!]\n");
	printf("-o=  --origin=\t\tSpecifying source of the archive [Only with -r!]\n");
	exit(0);
}

/* This function is called, when the user fires
 * the START!-Button..
 */
void Homecopy::start() {
	/* We save the old variable settings
	 * to restore the old settings later
	 * in init()
	 */
	if (userlistViewsel_old.isEmpty() && destlistViewsel_old.isEmpty()) {
		userlistViewsel_old = userlistViewsel;
		destlistViewsel_old = destlistViewsel;
	}
	
	// If we want to save the profile..
	if (restore == false) {
		// If there is not already an operation pending..
		if (!running) {
			setinfo(QString(tr("Preparing backup")), 15);
			// Only append domainname to username if it isn't already..
			if (!username.startsWith(domainname)) {
				username = userlistViewsel;
				username.prepend(domainname);
			}
			
			// When all needed data is collected
			if (!userlistViewsel.isEmpty() && !destlistViewsel.isEmpty()) {
				// and the LDAP return codes are OK..
				if (ldapret == 0x00 /* Freshly initialized */|| ldapret == 0x02 /* Password wrong */) {
					// Initializing password input
					bool ok;
					QString pwget;
					QInputDialog* inputDialog = new QInputDialog();
					inputDialog->setOptions(QInputDialog::NoButtons);
					// If the LDAP was not initialized..
					if (ldapret == 0x00) {
						pwget = tr("Enter password for ");
						pwget.append(userlistViewsel).append(":");
					}
					// Or something else (Password wrong..)
					else {
						pwget = tr("Please enter the password of ");
						pwget.append(userlistViewsel);
						pwget.append(tr(" again. The password was wrong."));
					}
					password =  inputDialog->getText(NULL ,tr("Enter password"),
					pwget , QLineEdit::EchoMode(QLineEdit::Password), NULL, &ok);
					
					if (ok && !password.isEmpty()) {
						ldap_checkconnthread->start();
					}
				}
				/* If the LDAP-Login was successful or 
				 * no connection was possible..
				 * Start to create the archive..
				 */
				else if (ldapret == 0x01 /* Login successful */|| ldapret == 0x03 /* No connetion possible */) {
					// Creating a hash of the password
					QCryptographicHash pwhasher(QCryptographicHash::Sha1);
					pwhasher.addData(password.toLatin1());
					password = pwhasher.result().toHex();
					password = password.left(10); // First 10 characters..
					
					running = true;
					
					//createArchive();
					createArchivethread->start();
				}
			}
		}
	}
	// If we want to restore..
	else {
		// If there isn't already an operation pending..
		if (!running) {
			// If the user just wants to check the archive..
			if (sb3c) {
				QString username = userlistViewsel;	// lanfoobar.7z
				username.chop(3); // Ripping of .7z -> lanfoobar
				userdirPath = username; // lanfoobar
				sevenzippath = userlistViewsel;	// lanfoobar.7z
				sevenzippath.prepend(destlistViewsel);	// F:\\lanfoobar.7z
				setinfo(QString(tr("Preparing check")), 15);
				extractArchivethread->start();
				return;
			}
			
			setinfo(QString(tr("Preparing restore")), 15);
			
			// When all needed data is collected
			if (!userlistViewsel.isEmpty() && !destlistViewsel.isEmpty()) {
				// and the LDAP return codes are OK..
				if (ldapret == 0x00 /* Freshly initialized */|| ldapret == 0x02 /* Password wrong */) {
					// Preparing variables for restore
					QString username = userlistViewsel;	// lanfoobar.7z
					username.chop(3); // Ripping of .7z -> lanfoobar
					userdirPath = username; // lanfoobar
					sevenzippath = userlistViewsel;	// lanfoobar.7z
					sevenzippath.prepend(destlistViewsel);	// F:\\lanfoobar.7z
					
					// Initializing password input
					bool ok;
					QString pwget;
					QInputDialog* inputDialog = new QInputDialog();
					inputDialog->setOptions(QInputDialog::NoButtons);
					// If LDAP was uninitialized..
					if (ldapret == 0x00) {
						pwget = tr("Enter password for ");
						pwget.append(userlistViewsel).append(":");
					}
					// If the password was wrong..
					else {
						pwget = tr("Please enter the password of ");
						pwget.append(userlistViewsel);
						pwget.append(tr(" again. The password was wrong."));
					}
					
					password =  inputDialog->getText(NULL ,tr("Enter password"),
					pwget , QLineEdit::EchoMode(QLineEdit::Password), NULL, &ok);
					if (ok && !password.isEmpty()) {
						ldap_checkconnthread->start();
					}
				}
				// If the login was successful or the connection not possible
				else if (ldapret == 0x01 /* Login successful */|| ldapret == 0x03 /* No connetion possible */) {
					// Creating a hash of the password
					QCryptographicHash pwhasher(QCryptographicHash::Sha1);
					pwhasher.addData(password.toLatin1());
					password = pwhasher.result().toHex();
					password = password.left(10); // First 10 characters..
					
					running = true;
					
					// Start to extract the archive / check it..
					extractArchivethread->start();
				}
			}
		}
	}
}
