/*	Copyright (c) 2012 Nathan-J. Hirschauer <nathan-jedidja.hirschauer@wien.gv.at>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy of this
 *	software and associated documentation files (the "Software"), to deal in the
 *	Software without restriction, including without limitation the rights to use, copy,
 *	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *	and to permit persons to whom the Software is furnished to do so, subject to the
 *	following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *	OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef HOMECOPY_H
#define HOMECOPY_H

#include "ui_homecopy.h"
#include <QtGlobal>
#include <QThread>

#if QT_VERSION < 0x050000
    #include <QtGui/QDialog>
#else
    #include <QtWidgets>
#endif

#include <QObject>
#include <QMessageBox>
#include <QInputDialog>
#include <QCryptographicHash>
#include <QTextStream>
#include <QProcess>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QFileInfoList>
#include <QSettings>

/* Checking defines and setting variables
 * according to the target system
 */

#if (defined(__unix__))
	#include <unistd.h> // Needed for usleep()
	#define LDAP_DEPRECATED 1
	#include <ldap.h>
	#define usleep(x) usleep(x*10)
#endif

#if (defined(WIN32))
	#define WINVER 0x0500
	/* Minimum supported OS is
	 * now NT5.0 (Windows 2000).
	 * Otherwise we would not be
	 * able to call ConvertSidtoSidString..
	 */
    #define WIN32_LEAN_AND_MEAN // Excluding a bunch of unneeded headers
	#include <windows.h> // Needed for Sleep()
	#include <winldap.h> // Needed for LDAP operations
	#include <ntldap.h> // Needed for LDAP operations
	#include <schnlsp.h> // Needed for LDAP operations
	#include <winreg.h> // Needed for registry operations
	#include <Sddl.h> // Needed for SID operations
	#include <aclapi.h> // ACL-API
	#define usleep(x) Sleep((x)) // Defining Sleep() as usleep()
#endif

// Declaring general variables
extern QString homePath;
extern QString currentPath;
extern QString destlistViewsel;
extern QString userlistViewsel;
extern QString sevenzippath;
extern QString userdirPath;
extern QString hostname;
extern QString username;
extern QString password;
extern QString ss;
extern bool hcpmsgboxret;

extern int progressBarValue;
extern int count_ulistclicked;
extern int count_dlistclicked;
extern int ldapret;
extern bool sb1c;	
extern bool sb2c;
extern bool sb3c;
extern bool restore;
extern bool console;

// Main class for UI
class Homecopy : public QMainWindow, public Ui::MainWindow {
		Q_OBJECT
		
		public:
			Homecopy (QMainWindow *parent = 0);
			~Homecopy();
			
		private slots:
			void setRootIndex(const QModelIndex&);
			void init();	// Initialization
			void Events();	// Eventhandler
			//void EnumUSB();	// Used for enumerating all USB-Devices
			void start(/*bool, bool, bool*/);	// Starting the whole process
			//void createArchive(); // For creating the archives
			//void extractArchive(); // For extracting data from the archives
			void ulistclicked(const QModelIndex &);
			void dlistclicked(const QModelIndex &);
			void setprogressBarValue(int progressBarValue);
			void setinfo(QString, int);
			//void ldapinfo(int);
			void openabout();
			void help();
			
		public slots:
			void hcpmsgbox(int, QString, QString, int);			
};

// Worker class - used with threads
class Worker : public QThread {
	Q_OBJECT
	
	public:
		Worker();
		~Worker();
		
	public slots:
		void createArchive();
		void extractArchive();
		void ldap_checkconn();
		
	signals:
		void finished();
		void error(QString err);
		void setinfo(QString, int);
		//void ldapinfo(int);
		void msgbox(int, QString, QString, int);
		
	private slots:
		void decompress(QString, QString);
		void shutdown();
		bool deltree(QStringList locations);
		#if (defined(WIN32))
		bool regop(bool /*0=compress,1=extract*/, QString/*directory*/); // Slot for registry Operations
		#endif
};
#endif //HOMECOPY_H
