/*	Copyright (c) 2012 Nathan-J. Hirschauer <nathan-jedidja.hirschauer@wien.gv.at>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy of this
 *	software and associated documentation files (the "Software"), to deal in the
 *	Software without restriction, including without limitation the rights to use, copy,
 *	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *	and to permit persons to whom the Software is furnished to do so, subject to the
 *	following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *	OTHER DEALINGS IN THE SOFTWARE.
 */

#include "ui_about.h"
#include "about.h"

AboutDlg::AboutDlg(QWidget *parent) : QDialog(parent), aboutui(new Ui::AboutDlg) {
	this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    aboutui->setupUi(this);
    aboutui->versionlabel->setText(QString(QString::number(VERSION).append(".").append(QString::number(PATCHLEVEL)).append(".").append(QString::number(SUBLEVEL))));
}

AboutDlg::~AboutDlg()
{
	delete aboutui;
}
